variable "address_prefix" {}
variable "chef_repo_dir" {}
variable "chef_vaults" {}

variable "chef_vault_env" {
  default = "_default"
}

variable "chef_version" {}
variable "environment" {}
variable "gitlab_com_zone_id" {}
variable "instance_type" {}
variable "location" {}
variable "resource_group_name" {}
variable "sidekiq_asap_count" {}
variable "sidekiq_asap_instance_type" {}
variable "sidekiq_besteffort_count" {}
variable "sidekiq_besteffort_instance_type" {}
variable "sidekiq_elasticsearch_count" {}
variable "sidekiq_elasticsearch_instance_type" {}
variable "sidekiq_pullmirror_count" {}
variable "sidekiq_pullmirror_instance_type" {}
variable "sidekiq_realtime_count" {}
variable "sidekiq_realtime_instance_type" {}
variable "source_image" {}
variable "ssh_key" {}
variable "ssh_user" {}
variable "subnet_id" {}
variable "tier" {}
