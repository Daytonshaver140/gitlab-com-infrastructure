resource "azurerm_availability_set" "logsearch" {
  name                         = "${format("logsearch-%v", var.environment)}"
  location                     = "${var.location}"
  managed                      = true
  platform_update_domain_count = 20
  platform_fault_domain_count  = 3
  resource_group_name          = "${var.resource_group_name}"
}

resource "azurerm_network_interface" "logsearch" {
  count                   = "${var.logsearch_datanode_count}"
  name                    = "${format("logsearch-%02d-%v-%v", count.index + 1, var.tier, var.environment)}"
  internal_dns_name_label = "${format("logsearch-%02d-%v-%v", count.index + 1, var.tier, var.environment)}"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "${format("logsearch-%02d-%v", count.index + 1, var.environment)}"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "${join(".", slice(split(".", var.address_prefix), 0, 3))}.${count.index + 101}"
  }
}

resource "aws_route53_record" "logsearch" {
  count   = "${var.logsearch_datanode_count}"
  zone_id = "${var.gitlab_net_zone_id}"
  name    = "${format("logsearch-%02d.%v.%v.gitlab.net.", count.index + 1, var.tier, var.environment == "prod" ? "prd" : var.environment)}"
  type    = "A"
  ttl     = "300"
  records = ["${azurerm_network_interface.logsearch.*.private_ip_address[count.index]}"]
}

data "template_file" "chef-bootstrap-logsearch" {
  count    = "${var.logsearch_datanode_count}"
  template = "${file("${path.root}/../../templates/chef-bootstrap-ssh-keys.tpl")}"

  vars {
    chef_repo_dir   = "${var.chef_repo_dir}"
    chef_vault_env  = "${var.chef_vault_env}"
    chef_vaults     = "${var.chef_vaults}"
    chef_vault_env  = "${var.chef_vault_env}"
    chef_version    = "${var.chef_version}"
    environment     = "${var.environment}"
    ip_address      = "${azurerm_network_interface.logsearch.*.private_ip_address[count.index]}"
    hostname        = "${format("logsearch-%02d.%v.%v.gitlab.net", count.index + 1, var.tier, var.environment == "prod" ? "prd" : var.environment)}"
    ssh_private_key = "${var.ssh_private_key}"
    ssh_user        = "${var.ssh_user}"
  }
}

resource "azurerm_managed_disk" "logsearch-datadisk-0" {
  count                = "${var.logsearch_datanode_count}"
  name                 = "${format("logsearch-%02d-%v-datadisk-0", count.index + 1, var.environment == "prod" ? "prd" : var.environment)}"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "logsearch-datadisk-1" {
  count                = "${var.logsearch_datanode_count}"
  name                 = "${format("logsearch-%02d-%v-datadisk-1", count.index + 1, var.environment == "prod" ? "prd" : var.environment)}"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "logsearch-datadisk-2" {
  count                = "${var.logsearch_datanode_count}"
  name                 = "${format("logsearch-%02d-%v-datadisk-2", count.index + 1, var.environment == "prod" ? "prd" : var.environment)}"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "logsearch-datadisk-3" {
  count                = "${var.logsearch_datanode_count}"
  name                 = "${format("logsearch-%02d-%v-datadisk-3", count.index + 1, var.environment == "prod" ? "prd" : var.environment)}"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "logsearch-datadisk-4" {
  count                = "${var.logsearch_datanode_count}"
  name                 = "${format("logsearch-%02d-%v-datadisk-4", count.index + 1, var.environment == "prod" ? "prd" : var.environment)}"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "logsearch-datadisk-5" {
  count                = "${var.logsearch_datanode_count}"
  name                 = "${format("logsearch-%02d-%v-datadisk-5", count.index + 1, var.environment == "prod" ? "prd" : var.environment)}"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "logsearch" {
  count                         = "${var.logsearch_datanode_count}"
  name                          = "${format("logsearch-%02d.%v.%v.gitlab.net", count.index + 1, var.tier, var.environment == "prod" ? "prd" : var.environment)}"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  availability_set_id           = "${azurerm_availability_set.logsearch.id}"
  network_interface_ids         = ["${azurerm_network_interface.logsearch.*.id[count.index]}"]
  primary_network_interface_id  = "${azurerm_network_interface.logsearch.*.id[count.index]}"
  vm_size                       = "${var.logsearch_datanode_instance_type}"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "${format("osdisk-logsearch-%02d-%v", count.index + 1, var.environment)}"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.logsearch-datadisk-0.*.name[count.index]}"
    managed_disk_id = "${azurerm_managed_disk.logsearch-datadisk-0.*.id[count.index]}"
    disk_size_gb    = "${azurerm_managed_disk.logsearch-datadisk-0.*.disk_size_gb[count.index]}"
    create_option   = "Attach"
    lun             = 0
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.logsearch-datadisk-1.*.name[count.index]}"
    managed_disk_id = "${azurerm_managed_disk.logsearch-datadisk-1.*.id[count.index]}"
    disk_size_gb    = "${azurerm_managed_disk.logsearch-datadisk-1.*.disk_size_gb[count.index]}"
    create_option   = "Attach"
    lun             = 1
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.logsearch-datadisk-2.*.name[count.index]}"
    managed_disk_id = "${azurerm_managed_disk.logsearch-datadisk-2.*.id[count.index]}"
    disk_size_gb    = "${azurerm_managed_disk.logsearch-datadisk-2.*.disk_size_gb[count.index]}"
    create_option   = "Attach"
    lun             = 2
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.logsearch-datadisk-3.*.name[count.index]}"
    managed_disk_id = "${azurerm_managed_disk.logsearch-datadisk-3.*.id[count.index]}"
    disk_size_gb    = "${azurerm_managed_disk.logsearch-datadisk-3.*.disk_size_gb[count.index]}"
    create_option   = "Attach"
    lun             = 3
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.logsearch-datadisk-4.*.name[count.index]}"
    managed_disk_id = "${azurerm_managed_disk.logsearch-datadisk-4.*.id[count.index]}"
    disk_size_gb    = "${azurerm_managed_disk.logsearch-datadisk-4.*.disk_size_gb[count.index]}"
    create_option   = "Attach"
    lun             = 4
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.logsearch-datadisk-5.*.name[count.index]}"
    managed_disk_id = "${azurerm_managed_disk.logsearch-datadisk-5.*.id[count.index]}"
    disk_size_gb    = "${azurerm_managed_disk.logsearch-datadisk-5.*.disk_size_gb[count.index]}"
    create_option   = "Attach"
    lun             = 5
  }

  os_profile {
    computer_name  = "${format("logsearch-%02d.%v.%v.gitlab.net", count.index + 1, var.tier, var.environment == "prod" ? "prd" : var.environment)}"
    admin_username = "${var.ssh_user}"
  }

  os_profile_linux_config {
    disable_password_authentication = true

    ssh_keys = {
      path     = "/home/${var.ssh_user}/.ssh/authorized_keys"
      key_data = "${file("${var.ssh_public_key}")}"
    }
  }

  provisioner "local-exec" {
    command = "${data.template_file.chef-bootstrap-logsearch.*.rendered[count.index]}"
  }

  provisioner "remote-exec" {
    inline = ["nohup bash -c 'sudo chef-client &'"]

    connection {
      type        = "ssh"
      host        = "${azurerm_network_interface.logsearch.*.private_ip_address[count.index]}"
      user        = "${var.ssh_user}"
      private_key = "${file("${var.ssh_private_key}")}"
      timeout     = "10s"
    }
  }
}
