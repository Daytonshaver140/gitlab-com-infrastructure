variable "oauth2_client_id_monitoring" {}
variable "oauth2_client_secret_monitoring" {}

variable "gitlab_net_zone_id" {}
variable "gitlab_com_zone_id" {}

variable "bootstrap_script_version" {
  default = 8
}

#############################
# Default firewall
# rule for allowing
# all protocols on all
# ports
#
# 10.251.x.x: all of dr
# 10.250.7.x: ops runner
# 10.250.10.x: chatops runner
# 10.250.12.x: release runner
###########################

variable "internal_subnets" {
  type    = "list"
  default = ["10.251.0.0/16", "10.250.7.0/24", "10.250.10.0/24", "10.250.12.0/24"]
}

variable "other_monitoring_subnets" {
  type = "list"

  # 10.226.1.0/24 is gstg monitoring subnet
  default = ["10.226.1.0/24"]
}

variable "monitoring_hosts" {
  type = "map"

  default = {
    "names" = ["prometheus", "prometheus-app"]
    "ports" = [9090, 9090]
  }
}

#### GCP load balancing

# The top level domain record for the GitLab deployment.
# For production this should be set to "gitlab.com"
# Note: Currently `gitlab.com` is set outside of terraform
#       because of the switchover.

variable "lb_fqdns" {
  type    = "list"
  default = []
}

##########
variable "lb_fqdns_altssh" {
  type    = "list"
  default = ["altssh.dr.gitlab.com"]
}

variable "lb_fqdns_pages" {
  type    = "list"
  default = ["*.pages.dr.gitlab.io"]
}

variable "lb_fqdns_bastion" {
  type    = "list"
  default = ["lb-bastion.dr.gitlab.com"]
}

variable "lb_fqdns_internal" {
  type    = "list"
  default = ["int.dr.gitlab.net"]
}

variable "lb_fqdns_internal_pgbouncer" {
  type    = "list"
  default = ["pgbouncer.int.dr.gitlab.net"]
}

variable "lb_fqdns_internal_patroni" {
  type    = "list"
  default = ["patroni.int.dr.gitlab.net"]
}

variable "lb_fqdns_internal_postgres_11" {
  type    = "list"
  default = ["postgres11.int.dr.gitlab.net"]
}

#
# For every name there must be a corresponding
# forwarding port range and health check port
#

variable "tcp_lbs" {
  type = "map"

  default = {
    "names"                  = ["http", "https", "ssh"]
    "forwarding_port_ranges" = ["80", "443", "22"]
    "health_check_ports"     = ["8001", "8002", "8003"]
  }
}

variable "tcp_lbs_internal" {
  type = "map"

  default = {
    "names"                  = ["http-internal", "https-internal", "ssh-internal"]
    "forwarding_port_ranges" = ["80", "443", "22"]
    "health_check_ports"     = ["8001", "8002", "8003"]
  }
}

variable "tcp_lbs_pages" {
  type = "map"

  default = {
    "names"                  = ["http", "https"]
    "forwarding_port_ranges" = ["80", "443"]
    "health_check_ports"     = ["8001", "8002"]
  }
}

variable "tcp_lbs_altssh" {
  type = "map"

  default = {
    "names"                      = ["https"]
    "forwarding_port_ranges"     = ["443"]
    "health_check_ports"         = ["8003"]
    "health_check_request_paths" = ["/-/available-ssh"]
  }
}

variable "tcp_lbs_bastion" {
  type = "map"

  default = {
    "names"                  = ["ssh"]
    "forwarding_port_ranges" = ["22"]
    "health_check_ports"     = ["80"]
  }
}

##################
# Network Peering
##################

variable "network_env" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-dr/global/networks/dr"
}

variable "peer_networks" {
  type = "map"

  default = {
    "names" = ["ops", "gstg", "gprd"]

    "links" = [
      "https://www.googleapis.com/compute/v1/projects/gitlab-ops/global/networks/ops",
      "https://www.googleapis.com/compute/v1/projects/gitlab-staging-1/global/networks/gstg",
      "https://www.googleapis.com/compute/v1/projects/gitlab-production/global/networks/gprd",
    ]
  }
}

######################

variable "base_chef_run_list" {
  default = "\"role[gitlab]\",\"recipe[gitlab_users::default]\",\"recipe[gitlab_sudo::default]\",\"recipe[gitlab-server::bashrc]\""
}

variable "empty_chef_run_list" {
  default = "\"\""
}

variable "dns_zone_name" {
  default = "gitlab.com"
}

variable "run_lists" {
  type = "map"

  default = {
    "prometheus"  = "\"role[gitlab]\",\"recipe[gitlab_users::default]\",\"recipe[gitlab_sudo::default]\",\"recipe[gitlab-server::bashrc]\""
    "performance" = "\"role[gitlab]\",\"recipe[gitlab_users::default]\",\"recipe[gitlab_sudo::default]\",\"recipe[gitlab-server::bashrc]\""
  }
}

variable "public_ports" {
  type = "map"

  default = {
    "api"         = []
    "bastion"     = [22]
    "console"     = []
    "consul"      = []
    "deploy"      = []
    "db"          = []
    "fe-lb"       = [22, 80, 443]
    "geodb"       = []
    "git"         = []
    "pubsubbeat"  = []
    "redis"       = []
    "sd-exporter" = []
    "sidekiq"     = []
    "thanos"      = []
    "stor"        = []
    "web"         = []
    "monitoring"  = []
  }
}

variable "environment" {
  default = "dr"
}

variable "format_data_disk" {
  default = "true"
}

variable "project" {
  default = "gitlab-dr"
}

variable "region" {
  default = "us-west1"
}

variable "chef_provision" {
  type        = "map"
  description = "Configuration details for chef server"

  default = {
    bootstrap_bucket  = "gitlab-dr-chef-bootstrap"
    bootstrap_key     = "gitlab-dr-bootstrap-validation"
    bootstrap_keyring = "gitlab-dr-bootstrap"

    server_url    = "https://chef.gitlab.com/organizations/gitlab/"
    user_name     = "gitlab-ci"
    user_key_path = ".chef.pem"
    version       = "12.22.5"
  }
}

variable "monitoring_cert_link" {
  default = "projects/gitlab-dr/global/sslCertificates/wildcard-dr-gitlab-net"
}

variable "data_disk_sizes" {
  type = "map"

  default = {
    "file"    = "16000"
    "pages"   = "16000"
    "patroni" = "6000"
  }
}

variable "machine_types" {
  type = "map"

  default = {
    "api"                   = "n1-standard-16"
    "bastion"               = "g1-small"
    "db"                    = "n1-highmem-64"
    "console"               = "n1-standard-1"
    "consul"                = "g1-small"
    "deploy"                = "n1-standard-2"
    "fe-lb"                 = "n1-standard-1"
    "geodb"                 = "n1-standard-8"
    "git"                   = "n1-standard-16"
    "monitoring"            = "n1-standard-8"
    "redis"                 = "n1-standard-2"
    "sd-exporter"           = "n1-standard-1"
    "sidekiq"               = "n1-standard-1"
    "sidekiq-asap"          = "n1-standard-8"
    "sidekiq-besteffort"    = "n1-standard-8"
    "sidekiq-elasticsearch" = "n1-standard-8"
    "sidekiq-import"        = "n1-standard-8"
    "sidekiq-pages"         = "n1-standard-8"
    "sidekiq-pipeline"      = "n1-standard-8"
    "sidekiq-pullmirror"    = "n1-standard-8"
    "sidekiq-realtime"      = "n1-highmem-16"
    "sidekiq-traces"        = "n1-standard-8"
    "stor"                  = "n1-standard-16"
    "thanos-store"          = "n1-highmem-8"
    "thanos-compact"        = "n1-standard-2"
    "web"                   = "n1-standard-8"

    # pages and share should eventually be upgraded
    # to n1-standard-32 for better IO.

    "stor-pages" = "n1-highmem-8"
  }
}

variable "node_count" {
  type = "map"

  default = {
    "api"                   = 1
    "bastion"               = 1
    "blackbox"              = 1
    "console"               = 1
    "consul"                = 3
    "deploy"                = 1
    "fe-lb"                 = 3
    "fe-lb-altssh"          = 1
    "fe-lb-pages"           = 1
    "geodb"                 = 1
    "git"                   = 1
    "pages"                 = 1
    "patroni"               = 3
    "postgres-11"           = 0
    "redis"                 = 3
    "sd-exporter"           = 1
    "sidekiq-asap"          = 0
    "sidekiq-besteffort"    = 1
    "sidekiq-elasticsearch" = 0
    "sidekiq-import"        = 0
    "sidekiq-pages"         = 0
    "sidekiq-pipeline"      = 0
    "sidekiq-pullmirror"    = 0
    "sidekiq-realtime"      = 0
    "sidekiq-traces"        = 0
    "thanos-compact"        = 1
    "thanos-store"          = 1
    "stor"                  = 1
    "multizone-stor"        = 31
    "web"                   = 3
    "prometheus"            = 1
    "prometheus-app"        = 1
  }
}

variable "subnetworks" {
  type = "map"

  default = {
    "fe-lb"        = "10.251.1.0/24"
    "fe-lb-pages"  = "10.251.2.0/24"
    "fe-lb-altssh" = "10.251.3.0/24"
    "bastion"      = "10.251.4.0/24"
    "patroni"      = "10.251.9.0/24"
    "postgres11"   = "10.251.10.0/24"
    "deploy"       = "10.251.14.0/24"
    "console"      = "10.251.16.0/24"
    "consul"       = "10.251.13.0/24"
    "monitoring"   = "10.251.17.0/24"
    "pubsubbeat"   = "10.251.18.0/24"
    "api"          = "10.251.22.0/24"
    "git"          = "10.251.23.0/24"
    "geodb"        = "10.251.24.0/24"
    "redis"        = "10.251.5.0/24"
    "sidekiq"      = "10.251.25.0/24"
    "stor"         = "10.251.28.0/24"
    "thanos-store" = "10.251.31.0/24"
    "web"          = "10.251.26.0/24"
    "pages"        = "10.251.30.0/24"

    "thanos-compact" = "10.251.32.0/24"
  }
}

variable "service_account_email" {
  type = "string"

  default = "terraform@gitlab-dr.iam.gserviceaccount.com"
}

variable "gcs_service_account_email" {
  type    = "string"
  default = "gitlab-object-storage@gitlab-dr.iam.gserviceaccount.com"
}

variable "gcs_postgres_backup_service_account" {
  type    = "string"
  default = "postgres-wal-archive@gitlab-dr.iam.gserviceaccount.com"
}

# Service account used to do automated backup testing
# in https://gitlab.com/gitlab-restore/postgres-dr
variable "gcs_postgres_restore_service_account" {
  type    = "string"
  default = "postgres-automated-backup-test@gitlab-restore.iam.gserviceaccount.com"
}

variable "gcs_postgres_backup_kms_key_id" {
  type    = "string"
  default = "projects/gitlab-dr/locations/global/keyRings/gitlab-secrets/cryptoKeys/dr-postgres-wal-archive"
}

variable "postgres_backup_retention_days" {
  type    = "string"
  default = "14"
}

variable "egress_ports" {
  type    = "list"
  default = []
}

variable "deploy_egress_ports" {
  type    = "list"
  default = []
}

#######################
# pubsubbeat config
#######################

variable "pubsubbeats" {
  type = "map"

  default = {
    "names"         = ["gitaly", "haproxy", "pages", "postgres", "production", "system", "workhorse", "rspec", "sidekiq", "api", "nginx", "gitlab-shell", "shell", "rails", "unstructured", "unicorn", "application", "registry", "redis", "consul", "runner"]
    "machine_types" = ["n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-1", "n1-standard-1", "n1-standard-1", "n1-standard-1", "n1-standard-1", "n1-standard-1"]
  }
}

### Object Storage Configuration

variable "versioning" {
  type    = "string"
  default = "true"
}

variable "artifact_age" {
  type    = "string"
  default = "30"
}

variable "upload_age" {
  type    = "string"
  default = "30"
}

variable "lfs_object_age" {
  type    = "string"
  default = "30"
}

variable "package_repo_age" {
  type    = "string"
  default = "30"
}

variable "storage_class" {
  type    = "string"
  default = "MULTI_REGIONAL"
}

variable "storage_log_age" {
  type    = "string"
  default = "7"
}

variable "gcs_storage_analytics_group_email" {
  type    = "string"
  default = "cloud-storage-analytics@google.com"
}

#################
# Monitoring whitelist
#################

#################
# Allow traffic from the ops
# network from grafana
#################

variable "monitoring_whitelist_prometheus" {
  type = "map"

  default = {
    # 10.250.3.x for the internal grafana
    # 10.250.11.x for the public grafana
    # 10.250.8.x for the ops prometheus servers
    #
    # Port 9090 is for prometheus
    # Ports 10900-10902 is for thanos
    "subnets" = ["10.250.3.0/24", "10.250.11.0/24", "10.250.8.0/24"]

    "ports" = ["9090", "10900", "10901", "10902"]
  }
}

variable "monitoring_whitelist_influxdb" {
  type = "map"

  default = {
    # 10.250.3.x for internal dashboards
    "subnets" = ["10.250.3.0/24"]
    "ports"   = ["8086"]
  }
}

variable "monitoring_whitelist_thanos" {
  type = "map"

  default = {
    # 10.250.8.x for the ops prometheus servers
    # 10.250.3.x for the internal grafana
    # 10.250.11.x for the public grafana
    "subnets" = ["10.250.3.0/24", "10.250.11.0/24", "10.250.8.0/24"]

    "ports" = ["10901", "10902"]
  }
}

#################
# Allow traffic from the ops
# network from the alerts manager
#################
variable "monitoring_whitelist_alerts" {
  type = "map"

  default = {
    # 10.250.8.x for the ops alerts servers
    "subnets" = ["10.250.8.0/24"]
    "ports"   = ["9093"]
  }
}

####################################
# Default log filters for stackdriver
#####################################

variable "sd_log_filters" {
  type = "map"

  default = {
    "exclude_logtypes" = "resource.type=\"gce_instance\" AND (labels.tag:\"workhorse\" OR labels.tag:\"rails\" OR labels.tag:\"workhorse\" OR labels.tag:\"gitaly\")"
  }
}
