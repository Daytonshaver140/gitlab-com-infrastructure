##################################
#
#  Web front-end for puma testing
#
#################################

resource "aws_route53_record" "puma" {
  zone_id = "${var.gitlab_com_zone_id}"
  name    = "pre-puma.gitlab.com"
  type    = "A"
  ttl     = "300"
  records = ["${module.web-puma.instance_public_ips[0]}"]
}

resource "aws_route53_record" "puma-alt" {
  zone_id = "${var.gitlab_com_zone_id}"
  name    = "pre-puma-alt.gitlab.com"
  type    = "A"
  ttl     = "300"
  records = ["${module.web-puma-alt.instance_public_ips[0]}"]
}

module "web-puma" {
  bootstrap_version     = "${var.bootstrap_script_version}"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-fe-web-puma]\""
  dns_zone_name         = "${var.dns_zone_name}"
  egress_ports          = "${var.egress_ports}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  ip_cidr_range         = "${var.subnetworks["web-puma"]}"
  machine_type          = "${var.machine_types["web"]}"
  name                  = "web-puma"
  node_count            = 1
  os_disk_type          = "pd-ssd"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["web-puma"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 443
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v1.0.5"
  tier                  = "sv"
  use_new_node_name     = true
  use_external_ip       = true
  vpc                   = "${module.network.self_link}"
}

module "web-puma-alt" {
  bootstrap_version     = "${var.bootstrap_script_version}"
  chef_provision        = "${var.chef_provision}"
  chef_run_list         = "\"role[${var.environment}-base-fe-web-puma-alt]\""
  dns_zone_name         = "${var.dns_zone_name}"
  egress_ports          = "${var.egress_ports}"
  environment           = "${var.environment}"
  health_check          = "tcp"
  subnetwork_name       = "${module.web-puma.google_compute_subnetwork_name}"
  machine_type          = "${var.machine_types["web"]}"
  name                  = "web-puma-alt"
  node_count            = 1
  os_disk_type          = "pd-ssd"
  project               = "${var.project}"
  public_ports          = "${var.public_ports["web-puma"]}"
  region                = "${var.region}"
  service_account_email = "${var.service_account_email}"
  service_port          = 443
  source                = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/generic-sv-with-group.git?ref=v1.0.5"
  tier                  = "sv"
  use_new_node_name     = true
  use_external_ip       = true
  vpc                   = "${module.network.self_link}"
}
