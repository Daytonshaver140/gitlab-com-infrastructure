resource "azurerm_public_ip" "HAProdLB" {
  name                         = "HAProdLB"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
}

resource "azurerm_lb" "FrontEndLBProd" {
  name                = "FrontEndLBProd"
  location            = "${var.location}"
  resource_group_name = "${var.resource_group_name}"

  frontend_ip_configuration {
    name                 = "FrontEndLBProdIPConfiguration"
    public_ip_address_id = "${azurerm_public_ip.HAProdLB.id}"
  }
}

resource "azurerm_lb_backend_address_pool" "FrontEndLBProd" {
  resource_group_name = "${var.resource_group_name}"
  loadbalancer_id     = "${azurerm_lb.FrontEndLBProd.id}"
  name                = "FrontEndProdPool"
}

resource "azurerm_lb_probe" "frontend-http" {
  resource_group_name = "${var.resource_group_name}"
  loadbalancer_id     = "${azurerm_lb.FrontEndLBProd.id}"
  name                = "http"
  port                = 80
  protocol            = "Tcp"
  interval_in_seconds = 5
  number_of_probes    = 2
}

resource "azurerm_lb_rule" "frontend-http" {
  resource_group_name            = "${var.resource_group_name}"
  loadbalancer_id                = "${azurerm_lb.FrontEndLBProd.id}"
  name                           = "http"
  protocol                       = "Tcp"
  frontend_port                  = 80
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.FrontEndLBProd.id}"
  backend_port                   = 80
  frontend_ip_configuration_name = "FrontEndLBProdIPConfiguration"
  load_distribution              = "SourceIPProtocol"
  probe_id                       = "${azurerm_lb_probe.frontend-http.id}"
}

resource "azurerm_lb_probe" "frontend-https" {
  resource_group_name = "${var.resource_group_name}"
  loadbalancer_id     = "${azurerm_lb.FrontEndLBProd.id}"
  name                = "https"
  port                = 443
  protocol            = "Tcp"
  interval_in_seconds = 5
  number_of_probes    = 2
}

resource "azurerm_lb_rule" "frontend-https" {
  resource_group_name            = "${var.resource_group_name}"
  loadbalancer_id                = "${azurerm_lb.FrontEndLBProd.id}"
  name                           = "https"
  protocol                       = "Tcp"
  frontend_port                  = 443
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.FrontEndLBProd.id}"
  backend_port                   = 443
  frontend_ip_configuration_name = "FrontEndLBProdIPConfiguration"
  load_distribution              = "SourceIPProtocol"
  probe_id                       = "${azurerm_lb_probe.frontend-https.id}"
}

resource "azurerm_lb_probe" "frontend-ssh" {
  resource_group_name = "${var.resource_group_name}"
  loadbalancer_id     = "${azurerm_lb.FrontEndLBProd.id}"
  name                = "ssh"
  port                = 22
  protocol            = "Tcp"
  interval_in_seconds = 5
  number_of_probes    = 2
}

resource "azurerm_lb_rule" "frontend-ssh" {
  resource_group_name            = "${var.resource_group_name}"
  loadbalancer_id                = "${azurerm_lb.FrontEndLBProd.id}"
  name                           = "ssh"
  protocol                       = "Tcp"
  frontend_ip_configuration_name = "FrontEndLBProdIPConfiguration"
  frontend_port                  = 22
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.FrontEndLBProd.id}"
  backend_port                   = 22
  load_distribution              = "SourceIPProtocol"
  probe_id                       = "${azurerm_lb_probe.frontend-ssh.id}"
}
