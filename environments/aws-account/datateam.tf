data "aws_iam_policy_document" "datateam-greenhouse-extract" {
  statement {
    sid = "1"

    actions = [
      "s3:ListBucket",
    ]

    resources = [
      "${aws_s3_bucket.datateam-greenhouse-extract.arn}",
    ]
  }

  statement {
    sid = "2"

    actions = [
      "s3:GetObject",
      "s3:PutObject",
    ]

    resources = [
      "${aws_s3_bucket.datateam-greenhouse-extract.arn}",
      "${aws_s3_bucket.datateam-greenhouse-extract.arn}/*",
    ]
  }
}

resource "aws_iam_user" "datateam-greenhouse-extract" {
  name = "datateam-greenhouse-extract"
}

resource "aws_iam_policy" "datateam-greenhouse-extract" {
  name   = "datateam-greenhouse-extract"
  path   = "/"
  policy = "${data.aws_iam_policy_document.datateam-greenhouse-extract.json}"
}

resource "aws_iam_user_policy_attachment" "datateam-greenhouse-extract" {
  user       = "${aws_iam_user.datateam-greenhouse-extract.name}"
  policy_arn = "${aws_iam_policy.datateam-greenhouse-extract.arn}"
}

resource "aws_s3_bucket" "datateam-greenhouse-extract" {
  bucket = "datateam-greenhouse-extract"
  acl    = "private"
}
