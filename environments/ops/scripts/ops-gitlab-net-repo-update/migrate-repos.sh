#!/usr/bin/env bash

# Migrate projects to ops
# This script will:
# - Archive the project on .com
# - Import the project into ops
# - Change the description of the .com project to say this is now a mirror
#
# The api doesn't allow to set up push mirrors yet, so you'll need to manually
# - Set up push mirror to .com from the ops project
# - Unarchive the project on .com after the import is completed

set -e

SRC_HOST="https://gitlab.com"
SRC_API_PATH="$SRC_HOST/api/v4"
DST_HOST="https://ops.gitlab.net"
DST_API_PATH="$DST_HOST/api/v4"

if [ -z $1 ]; then
    echo "Usage: $0 [group-id]"
    echo "Example: $0 gitlab-com/gl-infra/terraform-modules/google"
    exit 1
fi

# URL encode the group name
group=$(echo $1 | sed 's/\//%2f/g')
# This assumes the groups in the SRC and DST have the same full path
new_group_id=$(curl --fail -H "PRIVATE-TOKEN: $DST_PRIVATE_TOKEN" -s "$DST_API_PATH/groups/$group" | jq -r '.id')
if [ "$new_group_id" = "" ]; then
    echo "Group $1 doesn't exist in $DST_HOST. Create before running this script again."
fi

pages=$(curl --fail -H "PRIVATE-TOKEN: $SRC_PRIVATE_TOKEN" -sI "$SRC_API_PATH/groups/$group/projects" | perl -ne 'm/X-Total-Pages: (\d+)/ && print $1')

for i in $(seq 1 "$pages"); do
    for d in $(curl --fail -H "PRIVATE-TOKEN: $SRC_PRIVATE_TOKEN" -s "$SRC_API_PATH/groups/$group/projects?page=$i" | jq -r '.[] | "\(.id),\(.path),\(.http_url_to_repo)"'); do
        IFS=',' read id path url <<< "$d"
        echo "Archiving $path ($id)..."
        curl --fail -s -X POST -H "PRIVATE-TOKEN: $SRC_PRIVATE_TOKEN" "$SRC_API_PATH/projects/$id/archive"
        echo "\nCreating $path in $DST_HOST..."
        resp=$(curl --fail -s -X POST -H "PRIVATE-TOKEN: $DST_PRIVATE_TOKEN" -H "Content-Type: application/json" -d '{"namespace_id":'$new_group_id',"path":"'$path'","import_url":"'$url'"}' "$DST_API_PATH/projects")
        new_url=$(echo $resp | jq -r '.web_url')
        echo "Updating description of $path in $SRC_HOST..."
        curl --fail -s -X PUT -H "PRIVATE-TOKEN: $SRC_PRIVATE_TOKEN" -H "Content-Type: application/json" -d '{"description": "This is a mirror of '$new_url' All changes to this repository will be overwritten!"}' "$SRC_API_PATH/projects/$id"
        echo "Done"
    done
done
