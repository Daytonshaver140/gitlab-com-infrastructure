variable "location" {
  description = "The location"
}

variable "resource_group_name" {
  description = "The name of the resource group"
}

variable "subnet_id" {}
variable "first_user_username" {}
variable "first_user_password" {}
variable "gitlab_com_zone_id" {}

resource "azurerm_availability_set" "StorageStaging" {
  name                         = "StorageStaging"
  location                     = "${var.location}"
  managed                      = true
  platform_update_domain_count = 20
  platform_fault_domain_count  = 3
  resource_group_name          = "${var.resource_group_name}"
}

output "availability_set_id" {
  value = "${azurerm_availability_set.StorageStaging.id}"
}

### nfs-pages-staging-01
resource "azurerm_public_ip" "nfs-pages-staging-01-public-ip1" {
  name                         = "nfs-pages-staging-01-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "nfs-pages-staging-01"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "nfs-pages-staging-01" {
  name                    = "nfs-pages-staging-01"
  internal_dns_name_label = "nfs-pages-staging-01"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "nfs-pages-staging-01-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.133.2.161"
    public_ip_address_id          = "${azurerm_public_ip.nfs-pages-staging-01-public-ip1.id}"
  }
}

resource "azurerm_virtual_machine" "nfs-pages-staging-01" {
  name                          = "nfs-pages-staging-01"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs-pages-staging-01.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-pages-staging-01"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name  = "nfs-pages-staging-01"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"

    # We will use custom_data to bootstrap the instance
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name            = "from-pages-01-for-staging-pages-0"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-pages-01-for-staging-pages-0"
    create_option   = "Attach"
    lun             = 0
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-pages-01-for-staging-pages-1"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-pages-01-for-staging-pages-1"
    create_option   = "Attach"
    lun             = 1
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-pages-01-for-staging-pages-2"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-pages-01-for-staging-pages-2"
    create_option   = "Attach"
    lun             = 2
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-pages-01-for-staging-pages-3"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-pages-01-for-staging-pages-3"
    create_option   = "Attach"
    lun             = 3
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-pages-01-for-staging-pages-4"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-pages-01-for-staging-pages-4"
    create_option   = "Attach"
    lun             = 4
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-pages-01-for-staging-pages-5"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-pages-01-for-staging-pages-5"
    create_option   = "Attach"
    lun             = 5
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-pages-01-for-staging-pages-6"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-pages-01-for-staging-pages-6"
    create_option   = "Attach"
    lun             = 6
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-pages-01-for-staging-pages-7"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-pages-01-for-staging-pages-7"
    create_option   = "Attach"
    lun             = 7
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-pages-01-for-staging-pages-8"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-pages-01-for-staging-pages-8"
    create_option   = "Attach"
    lun             = 8
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-pages-01-for-staging-pages-9"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-pages-01-for-staging-pages-9"
    create_option   = "Attach"
    lun             = 9
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-pages-01-for-staging-pages-10"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-pages-01-for-staging-pages-10"
    create_option   = "Attach"
    lun             = 10
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-pages-01-for-staging-pages-11"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-pages-01-for-staging-pages-11"
    create_option   = "Attach"
    lun             = 11
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-pages-01-for-staging-pages-12"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-pages-01-for-staging-pages-12"
    create_option   = "Attach"
    lun             = 12
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-pages-01-for-staging-pages-13"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-pages-01-for-staging-pages-13"
    create_option   = "Attach"
    lun             = 13
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-pages-01-for-staging-pages-14"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-pages-01-for-staging-pages-14"
    create_option   = "Attach"
    lun             = 14
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-pages-01-for-staging-pages-15"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-pages-01-for-staging-pages-15"
    create_option   = "Attach"
    lun             = 15
    caching         = "ReadWrite"
  }
}

### nfs-file-08-staging
resource "azurerm_public_ip" "nfs-file-08-staging-public-ip1" {
  name                         = "nfs-file-08-staging-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "nfs-file-08-staging"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "nfs-file-08-staging" {
  name                    = "nfs-file-08-staging"
  internal_dns_name_label = "nfs-file-08-staging"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "nfs-file-08-staging-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.133.2.108"
    public_ip_address_id          = "${azurerm_public_ip.nfs-file-08-staging-public-ip1.id}"
  }
}

resource "azurerm_virtual_machine" "nfs-file-08-staging" {
  name                          = "nfs-file-08-staging"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs-file-08-staging.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-file-08-staging"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name  = "nfs-file-08-staging"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"

    # We will use custom_data to bootstrap the instance
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name            = "from-file-08-for-staging-file-08-0"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-file-08-for-staging-file-08-0"
    create_option   = "Attach"
    lun             = 0
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-file-08-for-staging-file-08-1"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-file-08-for-staging-file-08-1"
    create_option   = "Attach"
    lun             = 1
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-file-08-for-staging-file-08-2"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-file-08-for-staging-file-08-2"
    create_option   = "Attach"
    lun             = 2
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-file-08-for-staging-file-08-3"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-file-08-for-staging-file-08-3"
    create_option   = "Attach"
    lun             = 3
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-file-08-for-staging-file-08-4"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-file-08-for-staging-file-08-4"
    create_option   = "Attach"
    lun             = 4
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-file-08-for-staging-file-08-5"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-file-08-for-staging-file-08-5"
    create_option   = "Attach"
    lun             = 5
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-file-08-for-staging-file-08-6"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-file-08-for-staging-file-08-6"
    create_option   = "Attach"
    lun             = 6
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-file-08-for-staging-file-08-7"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-file-08-for-staging-file-08-7"
    create_option   = "Attach"
    lun             = 7
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-file-08-for-staging-file-08-8"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-file-08-for-staging-file-08-8"
    create_option   = "Attach"
    lun             = 8
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-file-08-for-staging-file-08-9"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-file-08-for-staging-file-08-9"
    create_option   = "Attach"
    lun             = 9
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-file-08-for-staging-file-08-10"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-file-08-for-staging-file-08-10"
    create_option   = "Attach"
    lun             = 10
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-file-08-for-staging-file-08-11"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-file-08-for-staging-file-08-11"
    create_option   = "Attach"
    lun             = 11
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-file-08-for-staging-file-08-12"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-file-08-for-staging-file-08-12"
    create_option   = "Attach"
    lun             = 12
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-file-08-for-staging-file-08-13"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-file-08-for-staging-file-08-13"
    create_option   = "Attach"
    lun             = 13
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-file-08-for-staging-file-08-14"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-file-08-for-staging-file-08-14"
    create_option   = "Attach"
    lun             = 14
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "from-file-08-for-staging-file-08-15"
    disk_size_gb    = "1024"
    managed_disk_id = "/subscriptions/c802e1f4-573f-4049-8645-4f735e6411b3/resourceGroups/NFS-Stage/providers/Microsoft.Compute/disks/from-file-08-for-staging-file-08-15"
    create_option   = "Attach"
    lun             = 15
    caching         = "ReadWrite"
  }
}
