variable "oauth2_client_id_monitoring" {}
variable "oauth2_client_secret_monitoring" {}

variable "gitlab_net_zone_id" {}
variable "gitlab_com_zone_id" {}
variable "gitlab_io_zone_id" {}

variable "bootstrap_script_version" {
  default = 8
}

#############################
# Default firewall
# rule for allowing
# all protocols on all
# ports
#
# 10.216.x.x: all of gprd
# 10.250.7.x: ops runner
# 10.250.8.11/32: nessus scanner
# 10.250.10.x: chatops runner
# 10.250.12.x: release runner
# 10.12.0.0/14: pod address range in gitlab-ops for runners
###########################

variable "internal_subnets" {
  type    = "list"
  default = ["10.216.0.0/13", "10.250.7.0/24", "10.250.8.11/32", "10.250.10.0/24", "10.250.12.0/24", "10.12.0.0/14"]
}

variable "other_monitoring_subnets" {
  type = "list"

  # 10.226.1.0/24: gstg
  # 10.251.17.0/24: dr
  default = ["10.226.1.0/24", "10.251.17.0/24"]
}

variable "monitoring_hosts" {
  type = "map"

  default = {
    "names" = ["alerts", "prometheus", "prometheus-app", "prometheus-db"]
    "ports" = [9093, 9090, 9090, 9090]
  }
}

#### GCP load balancing

# The top level domain record for the GitLab deployment.
# For production this should be set to "gitlab.com"
# Note: Currently `gitlab.com` is set outside of terraform
#       because of the switchover.

variable "lb_fqdns" {
  type    = "list"
  default = ["canary.gitlab.com"]
}

##########
variable "lb_fqdns_altssh" {
  type    = "list"
  default = ["altssh.gprd.gitlab.com"]
}

variable "lb_fqdns_registry" {
  type    = "list"
  default = ["registry.gitlab.com"]
}

variable "lb_fqdns_cny" {
  type    = "list"
  default = []
}

variable "lb_fqdns_pages" {
  type    = "list"
  default = ["*.pages.gprd.gitlab.io"]
}

variable "lb_fqdns_bastion" {
  type    = "list"
  default = ["lb-bastion.gprd.gitlab.com"]
}

variable "lb_fqdns_internal" {
  type    = "list"
  default = ["int.gprd.gitlab.net"]
}

variable "lb_fqdns_internal_pgbouncer" {
  type    = "list"
  default = ["pgbouncer.int.gprd.gitlab.net"]
}

variable "lb_fqdns_internal_patroni" {
  type    = "list"
  default = ["patroni.int.gprd.gitlab.net"]
}

variable "lb_fqdns_internal_postgres_11" {
  type    = "list"
  default = ["postgres11.int.gprd.gitlab.net"]
}

variable "lb_fqdns_contributors" {
  type    = "list"
  default = ["lb-contributors.gprd.gitlab.com"]
}

#
# For every name there must be a corresponding
# forwarding port range and health check port
#

variable "tcp_lbs" {
  type = "map"

  default = {
    "names"                  = ["http", "https", "ssh"]
    "forwarding_port_ranges" = ["80", "443", "22"]
    "health_check_ports"     = ["8001", "8002", "8003"]
  }
}

variable "tcp_lbs_internal" {
  type = "map"

  default = {
    "names"                  = ["http-internal", "https-internal", "ssh-internal"]
    "forwarding_port_ranges" = ["80", "443", "22"]
    "health_check_ports"     = ["8001", "8002", "8003"]
  }
}

variable "tcp_lbs_pages" {
  type = "map"

  default = {
    "names"                  = ["http", "https"]
    "forwarding_port_ranges" = ["80", "443"]
    "health_check_ports"     = ["8001", "8002"]
  }
}

variable "tcp_lbs_altssh" {
  type = "map"

  default = {
    "names"                      = ["https"]
    "forwarding_port_ranges"     = ["443"]
    "health_check_ports"         = ["8003"]
    "health_check_request_paths" = ["/-/available-ssh"]
  }
}

variable "tcp_lbs_registry" {
  type = "map"

  default = {
    "names"                  = ["http", "https"]
    "forwarding_port_ranges" = ["80", "443"]
    "health_check_ports"     = ["8001", "8002"]
  }
}

variable "tcp_lbs_cny" {
  type = "map"

  default = {
    "names"                  = []
    "forwarding_port_ranges" = []
    "health_check_ports"     = []
  }
}

variable "tcp_lbs_bastion" {
  type = "map"

  default = {
    "names"                  = ["ssh"]
    "forwarding_port_ranges" = ["22"]
    "health_check_ports"     = ["80"]
  }
}

variable "tcp_lbs_contributors" {
  type = "map"

  default = {
    "names"                  = ["https"]
    "forwarding_port_ranges" = ["443"]
    "health_check_ports"     = ["443"]
  }
}

##################
# Network Peering
##################

variable "network_env" {
  default = "https://www.googleapis.com/compute/v1/projects/gitlab-production/global/networks/gprd"
}

variable "peer_networks" {
  type = "map"

  default = {
    "names" = ["ops", "gstg", "dr"]

    "links" = [
      "https://www.googleapis.com/compute/v1/projects/gitlab-ops/global/networks/ops",
      "https://www.googleapis.com/compute/v1/projects/gitlab-staging-1/global/networks/gstg",
      "https://www.googleapis.com/compute/v1/projects/gitlab-dr/global/networks/dr",
    ]
  }
}

######################

variable "base_chef_run_list" {
  default = "\"role[gitlab]\",\"recipe[gitlab_users::default]\",\"recipe[gitlab_sudo::default]\",\"recipe[gitlab-server::bashrc]\""
}

variable "empty_chef_run_list" {
  default = "\"\""
}

variable "dns_zone_name" {
  default = "gitlab.com"
}

variable "run_lists" {
  type = "map"

  default = {
    "prometheus"  = "\"role[gitlab]\",\"recipe[gitlab_users::default]\",\"recipe[gitlab_sudo::default]\",\"recipe[gitlab-server::bashrc]\""
    "performance" = "\"role[gitlab]\",\"recipe[gitlab_users::default]\",\"recipe[gitlab_sudo::default]\",\"recipe[gitlab-server::bashrc]\""
  }
}

variable "public_ports" {
  type = "map"

  default = {
    "api"                = []
    "bastion"            = [22]
    "blackbox"           = []
    "consul"             = []
    "console"            = []
    "deploy"             = []
    "runner"             = []
    "db-dr"              = []
    "pgb"                = []
    "fe-lb"              = [22, 80, 443]
    "git"                = []
    "mailroom"           = []
    "patroni"            = []
    "pubsubbeat"         = []
    "redis"              = []
    "redis-sidekiq"      = []
    "redis-cache"        = []
    "registry"           = []
    "registry-analytics" = []
    "sidekiq"            = []
    "sd-exporter"        = []
    "stor"               = []
    "thanos"             = []
    "contributors"       = [80, 443]
    "web"                = []
    "web-pages"          = []
    "monitoring"         = []
    "influxdb"           = []
  }
}

variable "environment" {
  default = "gprd"
}

variable "format_data_disk" {
  default = "true"
}

variable "project" {
  default = "gitlab-production"
}

variable "region" {
  default = "us-east1"
}

variable "chef_provision" {
  type        = "map"
  description = "Configuration details for chef server"

  default = {
    bootstrap_bucket  = "gitlab-gprd-chef-bootstrap"
    bootstrap_key     = "gitlab-gprd-bootstrap-validation"
    bootstrap_keyring = "gitlab-gprd-bootstrap"

    server_url    = "https://chef.gitlab.com/organizations/gitlab/"
    user_name     = "gitlab-ci"
    user_key_path = ".chef.pem"
    version       = "12.22.5"
  }
}

variable "monitoring_cert_link" {
  default = "projects/gitlab-production/global/sslCertificates/wildcard-gprd-gitlab-net"
}

variable "data_disk_sizes" {
  type = "map"

  default = {
    "file"          = "16000"
    "share"         = "20000"
    "pages"         = "16000"
    "patroni"       = "4000"
    "prometheus"    = "4000"
    "prometheus-db" = "2000"
  }
}

variable "machine_types" {
  type = "map"

  default = {
    "alerts"                = "n1-standard-1"
    "api"                   = "n1-standard-16"
    "bastion"               = "g1-small"
    "blackbox"              = "n1-standard-1"
    "camoproxy"             = "n1-standard-1"
    "consul"                = "n1-standard-4"
    "contributors"          = "n1-standard-4"
    "contributors-db"       = "db-custom-1-4096"
    "db-dr"                 = "n1-standard-8"
    "console"               = "n1-standard-1"
    "deploy"                = "n1-standard-2"
    "fe-lb"                 = "n1-standard-4"
    "git"                   = "n1-standard-16"
    "influxdb"              = "n1-standard-8"
    "mailroom"              = "n1-standard-4"
    "monitoring"            = "n1-highmem-8"
    "patroni"               = "n1-highmem-64"
    "pgb"                   = "n1-standard-4"
    "redis"                 = "n1-standard-8"
    "redis-sidekiq"         = "n1-standard-8"
    "redis-cache"           = "n1-highmem-16"
    "redis-cache-sentinel"  = "n1-standard-1"
    "registry"              = "n1-standard-2"
    "registry-analytics"    = "n1-highmem-8"
    "runner"                = "n1-standard-2"
    "sd-exporter"           = "n1-standard-1"
    "sidekiq-asap"          = "custom-4-20480"
    "sidekiq-besteffort"    = "n1-standard-8"
    "sidekiq-elasticsearch" = "n1-standard-8"
    "sidekiq-import"        = "n1-standard-4"
    "sidekiq-pages"         = "n1-standard-4"
    "sidekiq-pipeline"      = "n1-standard-4"
    "sidekiq-pullmirror"    = "n1-standard-4"
    "sidekiq-realtime"      = "n1-standard-8"
    "sidekiq-traces"        = "n1-standard-8"
    "stor"                  = "n1-standard-32"
    "thanos-compact"        = "n1-standard-4"
    "thanos-store"          = "n1-highmem-32"
    "web"                   = "n1-standard-16"
    "web-pages"             = "n1-standard-8"

    # pages and share should eventually be upgraded
    # to n1-standard-32 for better IO.

    "stor-pages" = "n1-highmem-8"
    "stor-share" = "n1-highmem-8"
  }
}

variable "node_count" {
  type = "map"

  default = {
    "api"                   = 20
    "bastion"               = 3
    "blackbox"              = 1
    "camoproxy"             = 2
    "console"               = 1
    "consul"                = 5
    "db-dr"                 = 2
    "deploy"                = 1
    "deploy-cny"            = 1
    "fe-lb"                 = 16
    "fe-lb-altssh"          = 2
    "fe-lb-pages"           = 2
    "fe-lb-registry"        = 2
    "fe-lb-cny"             = 0
    "git"                   = 25
    "mailroom"              = 2
    "patroni"               = 7
    "postgres-11"           = 0
    "pages"                 = 1
    "pgb"                   = 3
    "redis"                 = 3
    "redis-sidekiq"         = 3
    "redis-cache"           = 3
    "redis-cache-sentinel"  = 3
    "registry"              = 4
    "registry-analytics"    = 1
    "runner"                = 1
    "share"                 = 1
    "sd-exporter"           = 1
    "sidekiq-asap"          = 10
    "sidekiq-besteffort"    = 8
    "sidekiq-elasticsearch" = 0
    "sidekiq-import"        = 4
    "sidekiq-pages"         = 12
    "sidekiq-pipeline"      = 6
    "sidekiq-pullmirror"    = 10
    "sidekiq-realtime"      = 8
    "sidekiq-traces"        = 0
    "stor"                  = 20
    "thanos-compact"        = 1
    "thanos-store"          = 2
    "contributors"          = 1
    "multizone-stor"        = 16
    "web"                   = 38
    "web-pages"             = 8
    "web-cny"               = 2
    "api-cny"               = 2
    "git-cny"               = 2
    "registry-cny"          = 2
    "alerts"                = 2
    "prometheus"            = 2
    "prometheus-app"        = 2
    "prometheus-db"         = 2
    "influxdb"              = 2
  }
}

variable "subnetworks" {
  type = "map"

  default = {
    "fe-lb"              = "10.216.1.0/24"
    "fe-lb-pages"        = "10.216.2.0/24"
    "fe-lb-altssh"       = "10.216.3.0/24"
    "fe-lb-registry"     = "10.216.5.0/24"
    "fe-lb-cny"          = "10.216.6.0/24"
    "bastion"            = "10.216.4.0/24"
    "redis"              = "10.217.2.0/24"
    "db-dr-delayed"      = "10.217.3.0/24"
    "db-dr-archive"      = "10.217.7.0/24"
    "patroni"            = "10.220.16.0/24"
    "postgres11"         = "10.220.19.0/24"
    "pgb"                = "10.217.4.0/24"
    "redis-cache"        = "10.217.5.0/24"
    "redis-sidekiq"      = "10.217.6.0/24"
    "consul"             = "10.218.1.0/24"
    "deploy"             = "10.218.3.0/24"
    "runner"             = "10.218.4.0/24"
    "console"            = "10.218.5.0/24"
    "deploy-cny"         = "10.218.7.0/24"
    "monitoring"         = "10.219.1.0/24"
    "pubsubbeat"         = "10.219.2.0/24"
    "registry"           = "10.220.10.0/23"
    "registry-analytics" = "10.218.6.0/24"
    "mailroom"           = "10.220.14.0/23"
    "api"                = "10.220.2.0/23"
    "git"                = "10.220.4.0/23"
    "singleton-svcs"     = "10.219.4.0/24"
    "sidekiq"            = "10.220.6.0/23"
    "thanos-compact"     = "10.220.18.0/24"
    "thanos-store"       = "10.220.17.0/24"
    "web"                = "10.220.8.0/23"
    "web-pages"          = "10.220.12.0/23"
    "stor"               = "10.221.2.0/23"
    "influxdb"           = "10.219.3.0/24"
    "camoproxy"          = "10.220.19.0/24"

    ###############################
    # These will eventually (tm) be
    # moved to object storage

    "pages" = "10.221.6.0/24"
    "share" = "10.221.7.0/24"

    #############################
  }
}

variable "service_account_email" {
  type = "string"

  default = "terraform@gitlab-production.iam.gserviceaccount.com"
}

variable "gcs_service_account_email" {
  type    = "string"
  default = "gitlab-object-storage-prd@gitlab-production.iam.gserviceaccount.com"
}

variable "gcs_postgres_backup_service_account" {
  type    = "string"
  default = "postgres-wal-archive@gitlab-production.iam.gserviceaccount.com"
}

# Service account used to do automated backup testing
# in https://gitlab.com/gitlab-restore/postgres-gprd
variable "gcs_postgres_restore_service_account" {
  type    = "string"
  default = "postgres-automated-backup-test@gitlab-restore.iam.gserviceaccount.com"
}

variable "gcs_postgres_backup_kms_key_id" {
  type    = "string"
  default = "projects/gitlab-production/locations/global/keyRings/gitlab-secrets/cryptoKeys/gprd-postgres-wal-archive"
}

variable "postgres_backup_retention_days" {
  type    = "string"
  default = "14"
}

variable "egress_ports" {
  type    = "list"
  default = []
}

variable "web_egress_ports" {
  type    = "list"
  default = []
}

variable "deploy_egress_ports" {
  type    = "list"
  default = []
}

variable "console_egress_ports" {
  type    = "list"
  default = []
}

variable "os_boot_image" {
  type = "map"

  default = {
    "camoproxy" = "ubuntu-os-cloud/ubuntu-1804-bionic-v20190404"
    "fe-lb"     = "ubuntu-os-cloud/ubuntu-1804-bionic-v20190404"
  }
}

####################################
# Camo proxy values
#####################################

variable "camoproxy_cert_link" {
  default = "projects/gitlab-production/global/sslCertificates/user-content-gitlab-static-net"
}

variable "camoproxy_domain" {
  type    = "string"
  default = "gitlab-static.net"
}

variable "camoproxy_serviceport" {
  type    = "string"
  default = 80       # Actually haproxy, or whatever is in front of camoproxy doing blacklisting
}

variable "camoproxy_hostname" {
  type    = "string"
  default = "user-content"
}

# This is permanent, regardless of any other egress port changes.
variable "camoproxy_egress_ports" {
  type    = "list"
  default = ["80", "443"]
}

# Supplied by env var
variable "gitlab_static_net_zone_id" {}

#######################
# pubsubbeat config
#######################

variable "pubsubbeats" {
  type = "map"

  default = {
    "names"         = ["gitaly", "haproxy", "pages", "postgres", "production", "system", "workhorse", "rspec", "sidekiq", "api", "nginx", "gitlab-shell", "shell", "rails", "unstructured", "unicorn", "application", "registry", "redis", "consul", "runner", "camoproxy"]
    "machine_types" = ["n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-2", "n1-standard-1", "n1-standard-1", "n1-standard-1", "n1-standard-1", "n1-standard-1", "n1-standard-1", "n1-standard-1"]
  }
}

### Object Storage Configuration

variable "versioning" {
  type    = "string"
  default = "true"
}

variable "artifact_age" {
  type    = "string"
  default = "30"
}

variable "upload_age" {
  type    = "string"
  default = "30"
}

variable "lfs_object_age" {
  type    = "string"
  default = "30"
}

variable "package_repo_age" {
  type    = "string"
  default = "30"
}

variable "storage_class" {
  type    = "string"
  default = "MULTI_REGIONAL"
}

variable "storage_log_age" {
  type    = "string"
  default = "7"
}

variable "gcs_storage_analytics_group_email" {
  type    = "string"
  default = "cloud-storage-analytics@google.com"
}

#################
# Monitoring whitelist
#################

#################
# Allow traffic from the ops
# network from grafana
#################

variable "monitoring_whitelist_prometheus" {
  type = "map"

  default = {
    # 10.250.3.x for the internal grafana
    # 10.250.11.x for the public grafana
    # 10.250.8.x for the ops prometheus servers
    #
    # Port 9090 is for prometheus
    # Ports 10900-10902 is for thanos
    "subnets" = ["10.250.3.0/24", "10.250.11.0/24", "10.250.8.0/24"]

    "ports" = ["9090", "10900", "10901", "10902"]
  }
}

variable "monitoring_whitelist_influxdb" {
  type = "map"

  default = {
    # 10.250.3.x for internal dashboards
    "subnets" = ["10.250.3.0/24"]
    "ports"   = ["8086"]
  }
}

variable "monitoring_whitelist_thanos" {
  type = "map"

  default = {
    # 10.250.8.x for the ops prometheus servers
    # 10.250.3.x for the internal grafana
    # 10.250.11.x for the public grafana
    "subnets" = ["10.250.3.0/24", "10.250.11.0/24", "10.250.8.0/24"]

    "ports" = ["10901", "10902"]
  }
}

#################
# Allow traffic from the ops
# network from the alerts manager
#################
variable "monitoring_whitelist_alerts" {
  type = "map"

  default = {
    # 10.250.8.x for the ops alerts servers
    "subnets" = ["10.250.8.0/24"]
    "ports"   = ["9093"]
  }
}

####################################
# Default log filters for stackdriver
#####################################

variable "sd_log_filters" {
  type = "map"

  default = {
    "exclude_logtypes" = "resource.type=\"gce_instance\" AND (labels.tag:\"workhorse\" OR labels.tag:\"rails\" OR labels.tag:\"workhorse\" OR labels.tag:\"gitaly\")"
  }
}
